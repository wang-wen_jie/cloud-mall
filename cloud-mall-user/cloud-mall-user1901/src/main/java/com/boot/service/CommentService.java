package com.boot.service;

import com.boot.pojo.Comment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentService {


    List<Comment> selectComment(long topicid, int page);


}
