package com.boot.dao;

import com.boot.pojo.Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/** @author 游政杰 */
@Mapper
@Repository
public interface CommentMapper {

    //根据topicid也就是productid和当前分页数来进行查询--默认size=3
    List<Comment> selectCommentByTopicAndPage(@Param("topicid") long topicid,
                                              @Param("page") int page);




}
