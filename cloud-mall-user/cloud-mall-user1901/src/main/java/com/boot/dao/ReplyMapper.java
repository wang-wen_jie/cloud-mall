package com.boot.dao;

import com.boot.pojo.SecondReply;
import com.boot.pojo.ThirdReply;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 游政杰
 * */
@Mapper
@Repository
public interface ReplyMapper {

    //查询指定评论下的二级评论
    List<SecondReply> selectSecondReply(@Param("commentid") long commentid, @Param("page") int page);

    //查询指定二级评论下的回复(三级回复)
    List<ThirdReply> selectThirdReply(@Param("rootReply") long rootReply); //根据root_reply即可查询



}
