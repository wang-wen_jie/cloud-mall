package com.boot.controller;

import com.boot.pojo.Comment;
import com.boot.service.CommentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

import java.util.List;

@Controller
@Api("评论控制器")
@RequestMapping(path = "/feign/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping(path = "/selectCommentByTopicid/{topicid}")
    @ResponseBody
    public List<Comment> selectCommentByTopicid(@PathVariable("topicid") long topicid)
    {
        List<Comment> comment = commentService.selectComment(topicid, 0);
        return comment;
    }



}
