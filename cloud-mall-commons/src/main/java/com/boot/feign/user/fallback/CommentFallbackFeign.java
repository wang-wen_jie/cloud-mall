package com.boot.feign.user.fallback;

import com.boot.feign.user.fallback.impl.CommentFallbackFeignImpl;
import com.boot.pojo.Comment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@FeignClient(value = "cloud-mall-user",fallback = CommentFallbackFeignImpl.class)
public interface CommentFallbackFeign {


    @GetMapping(path = "/feign/comment/selectCommentByTopicid/{topicid}")
    @ResponseBody
    public List<Comment> selectCommentByTopicid(@PathVariable("topicid") long topicid);



}
