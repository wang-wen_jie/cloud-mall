package com.boot.feign.user.fallback.impl;

import com.boot.feign.user.fallback.CommentFallbackFeign;
import com.boot.pojo.Comment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@Slf4j
public class CommentFallbackFeignImpl implements CommentFallbackFeign {

    @Override
    public List<Comment> selectCommentByTopicid(long topicid) {
        log.error("selectCommentByTopicid error");
        return null;
    }
}
