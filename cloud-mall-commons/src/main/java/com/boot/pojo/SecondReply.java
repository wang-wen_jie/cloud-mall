package com.boot.pojo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("二级回复")
public class SecondReply implements Serializable {

    private long id;
    private long commentid; //评论id
    private long rootReply;//该回复的-顶层-回复id(也就是这条评论是哪个二级评论下的)，如果=-1为自己就是顶层二级回复
    private String content; //内容
    private User fromUser; //回复者user对象，里面有id，名字，头像。。信息
    private String createTime; //回复时间
    private List<ThirdReply> thirdReplies;//三级评论

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCommentid() {
        return commentid;
    }

    public void setCommentid(long commentid) {
        this.commentid = commentid;
    }

    public long getRootReply() {
        return rootReply;
    }

    public void setRootReply(long rootReply) {
        this.rootReply = rootReply;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public List<ThirdReply> getThirdReplies() {
        return thirdReplies;
    }

    public void setThirdReplies(List<ThirdReply> thirdReplies) {
        this.thirdReplies = thirdReplies;
    }

    @Override
    public String toString() {
        return "SecondReply{" +
                "id=" + id +
                ", commentid=" + commentid +
                ", rootReply=" + rootReply +
                ", content='" + content + '\'' +
                ", fromUser=" + fromUser +
                ", createTime='" + createTime + '\'' +
                ", thirdReplies=" + thirdReplies +
                '}';
    }
}
