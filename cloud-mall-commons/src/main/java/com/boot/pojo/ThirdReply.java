package com.boot.pojo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

@ApiModel("多级回复")
public class ThirdReply implements Serializable {

    private long id;
    private long commentid; //评论id
    private long rootReply;//该回复的-顶层-回复id(也就是这条评论是哪个二级评论下的)，如果=-1为自己就是顶层二级回复
    private String content; //内容
    private User fromUser; //回复者user对象，里面有id，名字，头像。。信息
    private User toUser; //被回复者user对象
    private String createTime; //回复时间

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCommentid() {
        return commentid;
    }

    public void setCommentid(long commentid) {
        this.commentid = commentid;
    }

    public long getRootReply() {
        return rootReply;
    }

    public void setRootReply(long rootReply) {
        this.rootReply = rootReply;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "id=" + id +
                ", commentid=" + commentid +
                ", rootReply=" + rootReply +
                ", content='" + content + '\'' +
                ", fromUser=" + fromUser +
                ", toUser=" + toUser +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
