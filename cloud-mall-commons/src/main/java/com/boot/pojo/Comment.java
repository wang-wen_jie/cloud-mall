package com.boot.pojo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.List;

@ApiModel("一级评论")
public class Comment implements Serializable {

    private long id;
    private long topicid; //评论的话题，在这里也就是评论的--商品id--
    private String content;//内容
    private User user; //评论的用户
    private String createTime; //评论时间
    private List<SecondReply> secondReplies; //二级评论

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTopicid() {
        return topicid;
    }

    public void setTopicid(long topicid) {
        this.topicid = topicid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public List<SecondReply> getSecondReplies() {
        return secondReplies;
    }

    public void setSecondReplies(List<SecondReply> secondReplies) {
        this.secondReplies = secondReplies;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", topicid=" + topicid +
                ", content='" + content + '\'' +
                ", user=" + user +
                ", createTime='" + createTime + '\'' +
                ", secondReplies=" + secondReplies +
                '}';
    }
}
